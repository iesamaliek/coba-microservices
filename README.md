ini adalah sample code dari 5 series tutorial microservices dengan beberapa penyesuaian
http://www.springboottutorial.com/creating-microservices-with-spring-boot-part-1-getting-started
http://www.springboottutorial.com/creating-microservices-with-spring-boot-part-2-forex-microservice
http://www.springboottutorial.com/creating-microservices-with-spring-boot-part-3-currency-conversion-microservice
http://www.springboottutorial.com/microservices-with-spring-boot-part-4-ribbon-for-load-balancing
http://www.springboottutorial.com/microservices-with-spring-boot-part-5-eureka-naming-server

spring-boot versi 2.0.1
java 10
spring cloud Finchley RC1
feign REST client
ribbon load balancing
aureka naming server

Test Forex Microservice
GET to http://localhost:8000/currency-exchange/from/EUR/to/INR
GET to http://localhost:8100/currency-converter-feign/from/EUR/to/INR/quantity/10000

Lunch Eureka http://localhost:8761
